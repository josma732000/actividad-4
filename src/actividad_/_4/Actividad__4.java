/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad_._4;

/**
 *
 * @author Jose
 */
public class Actividad__4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Ejercicio 1
        function countTrue(arr) {
	  return arr.filter(x=>x==true).length;
       }
        //Ejercicio 2
        function redundant(str) {
		return () => str;
       }
        //Ejercicio 3
        const REGEXP = /^$/;
        
        //Ejercicio 4
        function possibleBonus(a, b) {
	return b - a <= 6 && b - a > 0
       }
        //Ejercicio 5
        function areaOfCountry(name, area) {
	return "" + name + " is " + (100*area/148940000).toFixed(2).toString() + "% of the total world's landmass";
       }
        
    }
    
}
